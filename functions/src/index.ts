import * as functions from 'firebase-functions';

export const redirect = functions.https.onRequest((request, response) => {
functions.logger.info("Redirect for request: ", request.headers["x-forwarded-proto"], request.headers["x-forwarded-host"], request.url);
	response.redirect(301, "https://blog.jdriven.com/")
});
